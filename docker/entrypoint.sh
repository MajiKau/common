#!/bin/sh

RELEASERC_PATH=${RELEASERC_PATH:-"$CI_PROJECT_DIR/.releaserc.json"}

if [ ! -f "$RELEASERC_PATH" ]; then 
    echo "[blre/common] using default release config (no config found in $RELEASERC_PATH)";
    cp /opt/blrevive/common/.releaserc.json $RELEASERC_PATH; 
else
    echo "[blre/common] using repository release config ($RELEASERC_PATH)";
fi

exec "$@"