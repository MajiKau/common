include(cmake/tools.cmake)

# enable folders
set_property(GLOBAL PROPERTY USE_FOLDERS ON)

option(BLRE_INSTALL_TO_BLR "Install module files to BLR directory" ON)

# set cached variables
set(BLR_DIRECTORY "C:\\Program Files (x86)\\Steam\\steamapps\\common\\blacklightretribution" CACHE STRING "Path to BLR installation")
set(BLR_BIN_DIR "${BLR_DIRECTORY}\\Binaries\\Win32" CACHE STRING "Path to BLR binary directory")
set(BLR_EXECUTABLE "FoxGame-win32-Shipping.exe" CACHE STRING "BLR filename")

function(install_dependencies project_name project_dir)
    # install blrevive package
    if(NOT ${project_name} STREQUAL "BLRevive")
        if(DEFINED ENV{BLREVIVE_VERSION})
            set(BLRE_VERSION "$ENV{BLREVIVE_VERSION}")
            string(REPLACE "-beta" "" BLRE_VERSION "${BLRE_VERSION}")
            CPMAddPackage(
                NAME BLRevive
                VERSION ${BLRE_VERSION}
                GIT_REPOSITORY https://gitlab.com/blrevive/blrevive
                GIT_TAG v${BLRE_VERSION}
            )
        else()
            CPMAddPackage(
                NAME BLRevive
                GIT_REPOSITORY https://gitlab.com/blrevive/blrevive
                GIT_TAG development
            )
        endif()

    endif()

    # include package file if exists
    if(EXISTS "${project_dir}/cmake/packages.cmake")
        include("${project_dir}/cmake/packages.cmake")
    endif()
    
	# move vendor targets to own folder in solution
    get_current_targets(_vendor_targets)
    move_targets_to_folder("vendor" _vendor_targets)
	
    set(${project_name}_VENDOR_TARGETS ${_vendor_targets} CACHE STRING "")
endfunction()

function(configure_blrv_debugging project_name)
    # add debugger configuration
    set_target_properties(${project_name} PROPERTIES
        VS_DEBUGGER_WORKING_DIRECTORY "${BLR_BIN_DIR}"
        VS_DEBUGGER_COMMAND "${BLR_BIN_DIR}\\${BLR_EXECUTABLE}"
    )
endfunction()

function(configure_blrv_target project_name project_dir)
    # create header and source file lists
    file(GLOB_RECURSE headers CONFIGURE_DEPENDS "${project_dir}/include/*.h")
    file(GLOB_RECURSE sources CONFIGURE_DEPENDS "${project_dir}/source/*.cpp")
    list(APPEND sources "${project_dir}/source/${project_name}.rc")
	
	set(${project_name}_HEADERS ${headers} CACHE STRING "")
	set(${project_name}_SOURCES ${sources} CACHE STRING "")
	

    # create shared library 
    add_library(${project_name} SHARED ${headers} ${sources})
    source_group(TREE "${project_dir}/include/${project_name}" PREFIX "Include" FILES ${headers})
    source_group(TREE "${project_dir}/source" PREFIX "Source" FILES ${sources})

    set_target_properties(${project_name} PROPERTIES CXX_STANDARD 17)
    
    if(${project_name} STREQUAL "BLRevive")
        set_target_properties(${project_name} PROPERTIES WINDOWS_EXPORT_ALL_SYMBOLS ON)
    endif()

    target_compile_options(${project_name} PRIVATE /bigobj /Zi)

    # include headers
    target_include_directories(${project_name} PUBLIC 
        $<BUILD_INTERFACE:${project_dir}/include>
        $<INSTALL_INTERFACE:include/${project_name}>
    )

    if(NOT ${project_name} STREQUAL "BLRevive")
        target_link_libraries(${project_name} debug BLRevive::BLRevive)
        target_link_libraries(${project_name} optimized BLRevive::BLRevive)
    endif()
endfunction()

function(configure_blrv_environment project_name project_dir)
    install_dependencies(${project_name} ${project_dir})
    configure_blrv_target(${project_name} ${project_dir})
    configure_blrv_debugging(${project_name})

    if(BLRE_INSTALL_TO_BLR)
        # copy build to BLR directory
        if(${project_name} STREQUAL "BLRevive")
            set(BLRV_OUT_PATH "${BLR_BIN_DIR}\\${project_name}.dll")
        else()
            set(BLRV_OUT_PATH "${BLR_BIN_DIR}\\Modules\\${project_name}.dll")
        endif()

        add_custom_command(TARGET ${project_name} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:${project_name}> "${BLRV_OUT_PATH}")
    endif()

    # add preprocessor definitions
    target_compile_definitions(${project_name} PRIVATE 
        $<$<CONFIG:DEBUG>:DEBUG>
        PROJECT_NAME="${project_name}")

    if(EXISTS "${project_dir}/cmake/blrevive.conf.cmake")
        include("${project_dir}/cmake/blrevive.conf.cmake")
    endif()

    # set as startup project
    if(${CMAKE_PROJECT_NAME} STREQUAL ${project_name})
        set_property(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR} PROPERTY VS_STARTUP_PROJECT ${project_name})
    endif()
endfunction()
