#!/bin/sh

PACKAGE_REGISTRY_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic"

function upload_pkg_file() {
    package=$1
    path=$2
    filename=$3

    curl -sS -f --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "$path" "${PACKAGE_REGISTRY_URL}/${package}/${filename}"
}

PKG_VERSION=$1
MODULE_PACKAGE="${CI_PROJECT_NAME}/${PKG_VERSION}"

upload_pkg_file "$MODULE_PACKAGE" "build/${MODULE_NAME}.lib" "${MODULE_NAME}.lib"
upload_pkg_file "$MODULE_PACKAGE" "build/${MODULE_NAME}.dll" "${MODULE_NAME}.dll"

if [ "$CI_PROJECT_NAME" == "blrevive" ]; then
    upload_pkg_file "$MODULE_PACKAGE" "loader/build/DINPUT8.dll" "DINPUT8.dll"
fi