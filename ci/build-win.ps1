Write-Host "[BLRE/Build] building $Env:CI_PROJECT_NAME"

$IS_BLREVIVE_PROJECT=$Env:CI_PROJECT_NAME -eq "blrevive"
$Env:CPM_SOURCE_CACHE="$Env:CI_BUILD_DIR\.cpm-cache"
$CMAKE_INSTALL_PREFIX=".\install-cache"
$Env:BLREVIVE_VERSION=Get-Content .\blrevive.version
$BLRE_PACKAGE_REGISTRY_URL="$Env:CI_API_V4_URL/projects/BLRevive%2FBLRevive/packages/generic"
$CMAKE_INSTALL_PKG_REGISTRY_URL="$BLRE_PACKAGE_REGISTRY_URL/install-cache/$Env:BLREVIVE_VERSION/install-cache.tar.gz"

Write-Host "[BLRE/Build] using BLRevive v$Env:BLREVIVE_VERSION"

# download blrevive install cache if building a module
if(-Not $IS_BLREVIVE_PROJECT) {    
    Write-Host "[BLRE/Build] pulling install cache from $CMAKE_INSTALL_PKG_REGISTRY_URL"

    try {
        $ProgressPreference = 'SilentlyContinue'
        Invoke-WebRequest -Uri "$CMAKE_INSTALL_PKG_REGISTRY_URL" -OutFile "install-cache.tar.gz" -UseBasicParsing
    } catch {
        Write-Error "[BLRE/build] error while downloading install cache: $_"
        Exit 1
    }
    
    tar -xzvf "install-cache.tar.gz"
    if(!$?) {
        Write-Error "[BLRE/build] error while extracting install cache!"
        Exit 1
    }
    
    Remove-Item install-cache.tar.gz
    $Env:CPM_USE_LOCAL_PACKAGES="ON"
}

Write-Host "[BLRE/Build] building project $Env:CI_PROJECT_NAME"

# generate cmake project files
& "$cmake" -A Win32 -B build -DCMAKE_INSTALL_PREFIX="$CMAKE_INSTALL_PREFIX" -DBUILD_TESTS=OFF -DWITH_PERF_TOOL=OFF -DENABLE_CLANG=OFF
if(!$?) {
    Write-Error "[BLRE/build] generating cmake project failed!"
    Exit 1
}

# compile project
& "$cmake" --build build --config Release
if(!$?) {
    Write-Error "[BLRE/build] compiling project failed!"
    Exit 1
}

# upload install cache if building BLRevive/BLRevive
if($IS_BLREVIVE_PROJECT) {
    Write-Host "[BLRE/Build] creating install cache"

    # build install cache
    & "$cmake" --build build --config Release --target Install
    if(!$?) {
        Write-Error "[BLRE/build] installing cmake project failed!"
        Exit 1
    }

    # compress install cache
    tar -czvf install-cache.tar.gz "$CMAKE_INSTALL_PREFIX"
    if(!$?) {
        Write-Error "[BLRE/build] creating install cache tar failed!"
        Exit 1
    }
    
    # upload install cache
    try {
        Invoke-RestMethod -Headers @{"JOB-TOKEN"="$CI_JOB_TOKEN"} -InFile install-cache.tar.gz -uri "$CMAKE_INSTALL_PKG_REGISTRY_URL" -Method put
    } catch { 
        Write-Error "[BLRE/build] failed to upload install cache: $_"
        Exit 1
    }
}
